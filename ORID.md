### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	This morning we had our usual code review and stand meeting, and summarized the past week's learning using a concept map; we learned HTTP Basic and Restful APl Introduction; in the afternoon we learned a new concept: Pair In the afternoon, we learned a new concept: Pair Programming, and in this mode, we carried out springboot learning and api development and practice in the afternoon.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Cheerful.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	The summarization of knowledge has strengthened my memory of learning, and the new format of Pair Programming has really strengthened the relationship and communication between us programmers, which is really meaningful in a way.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	I have always believed that teamwork and communication is a crucial part of development. Team harmony and smooth communication are important prerequisites for smooth development, and I will continue to work hard with this goal in mind.