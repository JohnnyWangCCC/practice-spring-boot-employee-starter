package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class EmployeeRepository {
    private Map<Integer, Employee> employees = new HashMap();

    public EmployeeRepository() {
        for (int i = 0; i < 20; i++) {
            employees.put(i, new Employee(i, "Lily", 20, "Female", 8000, i));
        }
    }

    public Map<Integer, Employee> getEmployees() {
        return employees;
    }
}
