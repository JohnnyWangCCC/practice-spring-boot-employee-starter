package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("companies")
public class CompanyController {

    private EmployeeRepository employeeRepository;
    private CompanyRepository companyRepository;

    public CompanyController(EmployeeRepository employeeRepository, CompanyRepository companyRepository) {
        this.employeeRepository = employeeRepository;
        this.companyRepository = companyRepository;
    }

    @GetMapping
    public List<Company> getCompanies() {
        return new ArrayList<>(companyRepository.getCompanies().values());
    }

    @GetMapping("/{companyId}")
    public Company getCompanyById(@PathVariable("companyId") int companyId) {
        return companyRepository.getCompanies().get(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getAllEmployeesByCompanyId(@PathVariable("companyId") int companyId) {
        return employeeRepository.getEmployees().values().stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesPageQuery(@RequestParam("page") int page, @RequestParam("size") int size) {
        return companyRepository.getCompanies()
                .values()
                .stream()
                .skip((long) (page - 1) * size)
                .limit(size).collect(Collectors.toList());
    }

    @PostMapping
    public void createCompany(@RequestBody Company company) {
        company.setId(nextId());
        companyRepository.getCompanies().put(nextId(), company);
    }

    @PutMapping("/{companyId}")
    public void updateCompanyById(@PathVariable("companyId") int companyId, @RequestParam("name") String name) {
        Optional.ofNullable(companyRepository.getCompanies().get(companyId))
                .ifPresent(company -> {
                    company.setName(name);
                });
    }

    @DeleteMapping("/{companyId}")
    public void deleteCompanyById(@PathVariable("companyId") int companyId) {
        companyRepository.getCompanies().remove(companyId);
    }

    private Integer nextId() {
        int maxId = companyRepository.getCompanies()
                .values()
                .stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }
}
